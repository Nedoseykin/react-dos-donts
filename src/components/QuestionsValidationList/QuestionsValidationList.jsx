import React from 'react';
import * as PropTypes from 'prop-types';

import List from '../List';
import ListItem from '../ListItem';
import QuestionValidationMessage from '../QuestionValidationMessage';

const QuestionsValidationList = ({ config }) => {
  const listItems = config.map(({ id, label, isValidSelector }) => (
    <ListItem key={id}>
      <QuestionValidationMessage label={label} isValidSelector={isValidSelector} />
    </ListItem>
  ));

  return (
    <List>
      {listItems}
    </List>
  );
};

QuestionsValidationList.propTypes = {
  config: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

QuestionsValidationList.defaultProps = {
  config: [],
};

export { QuestionsValidationList };
