import { connect } from 'react-redux';
import get from 'lodash/get';

import { Question } from './Question';
import { acChangeValue } from '../../store/actions';

const mapStateToProps = (state, { model }) => {
  return {
    value: get(state.data, model, ''),
  };
};

const mapDispatchToProps = {
  change: ({ target }) => acChangeValue({ model: target.name, value: target.value }),
};

export default connect(mapStateToProps, mapDispatchToProps)(Question);
