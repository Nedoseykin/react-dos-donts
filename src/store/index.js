import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';

import rootReducer from './reducers';

export const history = createBrowserHistory();

const middleware = [routerMiddleware(history), thunk];

export * from './actions';

export * from './selectors';

export function configureStore() {
  return  createStore(rootReducer(history), {}, composeWithDevTools(
    applyMiddleware(...middleware),
  ));
}
