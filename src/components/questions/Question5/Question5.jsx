import React from 'react';

import Question from '../../Question';

const Question5 = () => {
  return (
    <Question
      id="Question_5"
      label="Question 5"
      model="risk.question5"
    />
  )
};

export { Question5 };
