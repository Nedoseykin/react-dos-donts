import React from 'react';

import QuestionValidationList from '../../QuestionsValidationList';

import {
  question2IsValidMemoizedSelector,
  question3IsValidMemoizedSelector,
  question4IsValidMemoizedSelector,
} from '../../../store/selectors';

const validationListConfig = [
  { id: '1', label: 'Question 2', isValidSelector: question2IsValidMemoizedSelector },
  { id: '2', label: 'Question 3', isValidSelector: question3IsValidMemoizedSelector },
  { id: '3', label: 'Question 4', isValidSelector: question4IsValidMemoizedSelector },
];

const Question234ValidationList = () => (
  <QuestionValidationList config={validationListConfig} />
);

export { Question234ValidationList };
