import { handleActions } from 'redux-actions';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import set from 'lodash/set';
import unset from 'lodash/unset';
import {
  acAppReset,
  acChangeValue,
  acUpdateStore,
} from '../actions';

const initialState = {};

export default handleActions({
  [acChangeValue]: (state, { payload }) => {
    const { model, value } = payload;
    const newState = cloneDeep(state);
    set(newState, model, value);
    return newState;
  },

  [acUpdateStore]: (state, { payload }) => {
    if (!Array.isArray(payload)) {
      return state;
    }

    const newState = cloneDeep(state);

    payload.forEach(({ model, value }) => {
      if (value === undefined) {
        const storeModelValue = get(newState, model);
        if (storeModelValue !== undefined) {
          unset(newState, model);
        }
      } else {
        set(newState, model, value);
      }
    });

    return newState;
  },

  [acAppReset]: () => ({
    ...initialState,
  }),
}, initialState);
