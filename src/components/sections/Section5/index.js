import { connect } from 'react-redux';

import { Section5 } from './Section5';
import {
  allAreVisibleSelector,
  question2Selector,
  question3Selector,
  question4Selector,
  questionIsValidMemoizedSelectorCreator,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  allAreVisible: allAreVisibleSelector(state),

  question2IsValid: questionIsValidMemoizedSelectorCreator('question2', question2Selector)(state),
  question3IsValid: questionIsValidMemoizedSelectorCreator('question3', question3Selector)(state),
  question4IsValid: questionIsValidMemoizedSelectorCreator('question4', question4Selector)(state),
});

export default connect(mapStateToProps)(Section5);
