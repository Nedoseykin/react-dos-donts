import React, { useMemo } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './QuestionValidationMessage.module.scss';

const QuestionValidationMessage = ({ label, isValid }) => {
  const questionValidity = useMemo(() => `${label} is ${isValid ? 'valid' : 'invalid'}`,
    [label, isValid]);

  const messageClassName = useMemo(() => classNames(
    styles.QuestionValidationMessage,
    isValid ? styles.validMessage : styles.invalidMessage,
  ), [isValid]);

  return (
    <h4 className={messageClassName}>
      {questionValidity}
    </h4>
  )
};

QuestionValidationMessage.propTypes = {
  label: PropTypes.string,
  isValid: PropTypes.bool,
};

QuestionValidationMessage.defaultProps = {
  label: '',
  isValid: false,
};

export { QuestionValidationMessage };
