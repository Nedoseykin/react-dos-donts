import React from 'react';
import PropTypes from 'prop-types';

import styles from './LinkAsAButton.module.scss';

const LinkAsAButton = ({ to, children }) => {
  return (
      <a
        className={styles.root}
        href={to}
        target="_self"
      >
        {children}
      </a>
  )
};

LinkAsAButton.propTypes = {
  to: PropTypes.string,
};

LinkAsAButton.defaultProps = {
  to: '#',
};

export default LinkAsAButton;
