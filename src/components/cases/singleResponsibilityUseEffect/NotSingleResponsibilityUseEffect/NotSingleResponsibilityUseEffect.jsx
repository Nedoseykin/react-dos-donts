import React, { useEffect, useState, useCallback } from 'react';
import { useLocation } from 'react-router';

import LinkAsAButton from '../../linkOrButton/LinkAsAButton/LinkAsAButton';

import { callApi } from '../../../../utils';

import styles from './NotSingleResponsibilityUseState.module.scss';

const NotSingleResponsibilityUseEffect = () => {
  const location = useLocation();

  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [locationInfo, setLocationInfo] = useState(location.pathname);

  const updateLocationInfo = useCallback(() => {
    console.log('%c%s', 'color: blue;', 'Not single useEffect. Update location info...');

    setLocationInfo(location.pathname);
  }, [location]);

  const loadData = useCallback(() => {
    console.log('%c%s', 'color: blue;', 'Not single useEffect. Load data...');

    setIsLoading(true);
    callApi()
      .then((response) => {
        setData(response);
        setError(null);
      })
      .catch((err) => {
        setError(err);
        setData(null);
      })
      .finally(() => {
        setIsLoading(false);
      })
  }, [setIsLoading]);

  useEffect(() => {
    updateLocationInfo();

    loadData();
  }, [updateLocationInfo, loadData]);

  return (
    <div className={styles.root}>
      <div className={styles.block}>
        Location info: {locationInfo}
      </div>

      <div className={styles.block}>
        Data: {isLoading ? 'Loading' : data || error}
      </div>

      <LinkAsAButton to="#">
        Go to #
      </LinkAsAButton>
    </div>
  )
};

export default NotSingleResponsibilityUseEffect;
