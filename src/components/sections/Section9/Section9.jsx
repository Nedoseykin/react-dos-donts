import React from 'react';
import * as PropTypes from 'prop-types';

import Section from '../../Section';
import SubSection from '../../SubSection';
import Question1 from '../../questions/Question1';
import Question2 from '../../questions/Question2';
import Question3 from '../../questions/Question3';
import Question4 from '../../questions/Question4';
import Question5 from '../../questions/Question5';
import Question234ValidationList from '../../questionValidationLists/Questions234ValidationList';

const Section9 = ({ allAreVisible }) => {
  return (
    <Section title="Section 9 - logic for working with fragments and cleansing rules is in the reducer level">
      <Question1 withCleansing={true} />

      <SubSection condition={allAreVisible}>
        <Question2 withFragments={true} />

        <Question3 withFragments={true} />

        <Question4 withFragments={true} />

        <SubSection>
          <Question234ValidationList />
        </SubSection>
      </SubSection>

      <Question5 />
    </Section>
  );
};

Section9.propTypes = {
  question1: PropTypes.string,
};

export { Section9 };
