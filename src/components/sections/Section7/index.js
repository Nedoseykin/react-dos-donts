import { connect } from 'react-redux';

import { Section7 } from './Section7';
import { allAreVisibleSelector } from '../../../store/selectors';

const mapStateToProps = state => ({
  allAreVisible: allAreVisibleSelector(state),
});

export default connect(mapStateToProps)(Section7);
