import React, { useRef } from 'react';
import PropTypes from 'prop-types';

import styles from './NoNeedUseStateCorrect.module.scss';

const NoNeedUseStateCorrect = ({ submit }) => {
  const count = useRef(0);

  return (
    <section className={styles.root}>
      <h3>useRef was used</h3>

      <div className={styles.toolbar}>
        <button onClick={() => count.current++}>
          Increment
        </button>

        <button onClick={() => submit(count.current)}>
          Submit
        </button>
      </div>
    </section>
  )
};

NoNeedUseStateCorrect.propTypes = {
  submit: PropTypes.func,
};

NoNeedUseStateCorrect.defaultProps = {
  submit: () => {},
};

export default NoNeedUseStateCorrect;
