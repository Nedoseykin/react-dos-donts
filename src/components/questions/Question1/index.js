import { connect } from 'react-redux';

import { acChangeQuestion1Value } from '../../../store/actions';

import { Question1 } from './Question1';

const mapDispatchToProps = {
  onChange: ({ target }) => acChangeQuestion1Value({ value: target.value }),
};

export default connect(null, mapDispatchToProps)(Question1);
