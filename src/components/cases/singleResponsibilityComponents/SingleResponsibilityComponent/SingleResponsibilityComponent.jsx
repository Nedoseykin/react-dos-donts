import React from 'react';
import PropTypes from 'prop-types';

import { DEVELOPMENT, PRODUCTION } from '../../../../constants/modes.constants';

import styles from './SingleResponsibilityComponent.module.scss';

const SingleResponsibilityComponent = ({ mode }) => {
  return (
    <>
      <h2>Currently application is working in </h2>
      {
        mode === DEVELOPMENT
          ? <h3 className={styles.development}>Development mode</h3>
          : <h3 className={styles.production}>Production mode</h3>
      }
    </>
  )
};

SingleResponsibilityComponent.propTypes = {
  mode: PropTypes.oneOf([DEVELOPMENT, PRODUCTION]),
};

SingleResponsibilityComponent.defaultProps = {
  mode: DEVELOPMENT,
};

export default SingleResponsibilityComponent;
