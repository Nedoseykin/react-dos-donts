import React from 'react';
import * as PropTypes from 'prop-types';

import Section from '../../Section';
import SubSection from '../../SubSection';

import NotSingleResponsibilityComponent
  from '../../cases/singleResponsibilityComponents/NotSingleResponsibilityComponent/NotSingleResponsibilityComponent';
import SingleResponsibilityComponent
  from '../../cases/singleResponsibilityComponents/SingleResponsibilityComponent/SingleResponsibilityComponent';

import { DEVELOPMENT, PRODUCTION } from '../../../constants/modes.constants';

const Section13 = () => {
  return (
    <Section title="Section 13 - Single responsibility component">
      <SubSection>
        <h3>Not a single responsibility component</h3>

        <NotSingleResponsibilityComponent mode={DEVELOPMENT} />
      </SubSection>

      <SubSection>
        <h3>Single responsibility component</h3>

        <SingleResponsibilityComponent mode={PRODUCTION} />
      </SubSection>
    </Section>
  );
};

Section13.propTypes = {
  question1: PropTypes.string,
};

export { Section13 };
