import { createAction } from 'redux-actions';
import {
  APP_RESET,
  CHANGE_QUESTION1_VALUE,
  CHANGE_VALUE,
  SET_ASIDE_IS_OPENED,
  TOGGLE_ASIDE_IS_OPENED,
  UPDATE_STORE,
} from './actionTypes';

export const acAppReset = createAction(APP_RESET);

export const acSetAsideIsOpened = createAction(SET_ASIDE_IS_OPENED);
export const acToggleAsideIsOpened = createAction(TOGGLE_ASIDE_IS_OPENED);

export const acChangeValue = createAction(CHANGE_VALUE);
export const acUpdateStore = createAction(UPDATE_STORE);

export const acChangeQuestion1Value = createAction(CHANGE_QUESTION1_VALUE);
