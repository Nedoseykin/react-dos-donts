import React from 'react';
import * as PropTypes from 'prop-types';

import Question from '../../Question';

const Question1UpdateStore = ({
  onUpdateStore,
  question2Fragments,
  question3Fragments,
  question4Fragments,
  withCleansing,
}) => {
  const handleChange = ({ target }) => {
    const { value } = target;

    const payload = [
      { model: 'risk.question2' },
      { model: 'risk.question3' },
      { model: 'risk.question4' },
    ];

    if (value === 'all') {
      [
        question2Fragments,
        question3Fragments,
        question4Fragments,
      ]
        .forEach((value, i) => { payload[i].value = value; })
    }

    onUpdateStore(payload);
  };

  return (
    <Question
      id="Question_1"
      label="Question 1"
      model="risk.question1"
      onChange={withCleansing ? handleChange : null}
    />
  )
};

Question1UpdateStore.propTypes = {
  withCleansing: PropTypes.bool,
  question2Fragments: PropTypes.string,
  question3Fragments: PropTypes.string,
  question4Fragments: PropTypes.string,
  onUpdateStore: PropTypes.func.isRequired,
};

Question1UpdateStore.defaultProps = {
  withCleansing: false,
};

export { Question1UpdateStore };
