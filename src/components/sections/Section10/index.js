import { connect } from 'react-redux';

import { Section10 } from './Section10';

const mapDispatchToProps = {
  submit: value => {
    console.log('Submit: ', value);
    return { type: 'SUBMIT', payload: { value } };
  },
};

export default connect(null, mapDispatchToProps)(Section10);
