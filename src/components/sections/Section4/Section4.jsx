import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import Section from '../../Section';
import Question from '../../Question';

import styles from './Section4.module.scss';

const Section4 = ({
  allAreVisible,
  question2IsValid,
  question3IsValid,
  question4IsValid,
}) => {
  const getQuestionValidity = (label, isValid) => `${label} is ${isValid ? 'valid' : 'invalid'}`;

  const question2Validity = getQuestionValidity('Question 2', question2IsValid);
  const question3Validity = getQuestionValidity('Question 3', question3IsValid);
  const question4Validity = getQuestionValidity('Question 4', question4IsValid);

  const getMessageClassName = isValid => classNames(
    styles.messageItem,
    isValid ? styles.validMessage : styles.invalidMessage,
  );

  return (
    <Section title="Section 4 - with questionIsValidSelector">
      <Question
        id="Question_1"
        label="Question 1"
        model="risk.question1"
      />

      {
        allAreVisible && (
          <>
            <Question
              id="Question_2"
              label="Question 2"
              model="risk.question2"
            />

            <Question
              id="Question_3"
              label="Question 3"
              model="risk.question3"
            />

            <Question
              id="Question_4"
              label="Question 4"
              model="risk.question4"
            />

            <div className={styles.message_box}>
              <h4 className={getMessageClassName(question2IsValid)}>
                {question2Validity}
              </h4>
              <h4 className={getMessageClassName(question3IsValid)}>
                {question3Validity}
              </h4>
              <h4 className={getMessageClassName(question4IsValid)}>
                {question4Validity}
              </h4>
            </div>
          </>
        )
      }

      <Question
        id="Question_5"
        label="Question 5"
        model="risk.question5"
      />
    </Section>
  );
};

Section4.propTypes = {
  question1: PropTypes.string,
};

export { Section4 };
