import React, { useEffect, useState, useCallback } from 'react';
import { useLocation } from 'react-router';

import LinkAsAButton from '../../linkOrButton/LinkAsAButton/LinkAsAButton';

import { callApi } from '../../../../utils';

import styles from './SingleResponsibilityUseState.module.scss';

const SingleResponsibilityUseEffect = () => {
  const location = useLocation();

  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [locationInfo, setLocationInfo] = useState(location.pathname);

  const updateLocationInfo = useCallback(() => {
    console.log('%c%s', 'color: green;', 'Single responsibility useState. Update location info...')

    setLocationInfo(location.pathname);
  }, [location]);

  const loadData = useCallback(() => {
    console.log('%c%s', 'color: green;', 'Single responsibility useState. Load data...');

    setIsLoading(true);
    callApi()
      .then((response) => {
        setData(response);
        setError(null);
      })
      .catch((err) => {
        setError(err);
        setData(null);
      })
      .finally(() => {
        setIsLoading(false);
      })
  }, [setIsLoading]);

  useEffect(() => {

    updateLocationInfo();
  }, [updateLocationInfo]);

  useEffect(() => {
      loadData();
  }, [loadData]);

  return (
    <div className={styles.root}>
      <div className={styles.block}>
        Location info: {locationInfo}
      </div>

      <div className={styles.block}>
        Data: {isLoading ? 'Loading' : data || error}
      </div>

      <LinkAsAButton to="#">
        Go to #
      </LinkAsAButton>
    </div>
  )
};

export default SingleResponsibilityUseEffect;
