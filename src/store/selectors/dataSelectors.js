import get from 'lodash/get';
import { createSelector } from 'reselect';
import { getFragmentsModel } from '../../utils';

const question1Selector = state => get(state, 'data.risk.question1');
const question2Selector = state => get(state, 'data.risk.question2');
const question3Selector = state => get(state, 'data.risk.question3');
const question4Selector = state => get(state, 'data.risk.question4');

const allAreVisibleSelector = state => question1Selector(state) === 'all';

const questionIsValidSelectorCreator = (question, selector) => (state) => {
  question && console.log('questionIsValidSelector: ', question);

  return Boolean(selector(state));
};

const question2IsValidSelector = questionIsValidSelectorCreator('question2', question2Selector);
const question3IsValidSelector = questionIsValidSelectorCreator('question3', question3Selector);
const question4IsValidSelector = questionIsValidSelectorCreator('question4', question4Selector);

const questionIsValidMemoizedSelectorCreator = (question, selector) => createSelector(
  [selector],
  (value) => {
    question && console.log('questionIsValidMemoizedSelector: ', question);

    return Boolean(value);
  },
);

const question2IsValidMemoizedSelector = questionIsValidMemoizedSelectorCreator(
  'question2',
  questionIsValidSelectorCreator('', question2Selector),
);
const question3IsValidMemoizedSelector = questionIsValidMemoizedSelectorCreator(
  'question3',
  questionIsValidSelectorCreator('', question3Selector),
);
const question4IsValidMemoizedSelector = questionIsValidMemoizedSelectorCreator(
  'question4',
  questionIsValidSelectorCreator('', question4Selector),
);

// fragments selectors
const question2FragmentsSelector = state => get(state.data, getFragmentsModel('risk.question2'));
const question3FragmentsSelector = state => get(state.data, getFragmentsModel('risk.question3'));
const question4FragmentsSelector = state => get(state.data, getFragmentsModel('risk.question4'));

export {
  question1Selector,
  question2Selector,
  question3Selector,
  question4Selector,

  allAreVisibleSelector,

  question2IsValidSelector,
  question3IsValidSelector,
  question4IsValidSelector,

  questionIsValidMemoizedSelectorCreator,

  question2IsValidMemoizedSelector,
  question3IsValidMemoizedSelector,
  question4IsValidMemoizedSelector,

  question2FragmentsSelector,
  question3FragmentsSelector,
  question4FragmentsSelector,
};
