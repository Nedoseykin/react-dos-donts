import { connect } from 'react-redux';

import { Section6 } from './Section6';
import {
  allAreVisibleSelector,
  question2IsValidMemoizedSelector,
  question3IsValidMemoizedSelector,
  question4IsValidMemoizedSelector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  allAreVisible: allAreVisibleSelector(state),
  question2IsValid: question2IsValidMemoizedSelector(state),
  question3IsValid: question3IsValidMemoizedSelector(state),
  question4IsValid: question4IsValidMemoizedSelector(state),
});

export default connect(mapStateToProps)(Section6);
