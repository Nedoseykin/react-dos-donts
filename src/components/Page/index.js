import { connect } from 'react-redux';

import { Page } from './Page';

import { asideIsOpenedSelector, pathNameSelector } from '../../store';

const mapStateToProps = state => ({
  pathName: pathNameSelector(state),
  asideIsOpened: asideIsOpenedSelector(state),
});

export default connect(mapStateToProps)(Page);
