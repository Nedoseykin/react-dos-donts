import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import Header from '../Header';
import Body from '../Body';
import Router from '../Router';

import { configureStore } from '../../store';
import { history } from '../../store';

import './App.scss';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div className="App">
          <Header>
            Header
          </Header>
          <Body>
            <Router />
          </Body>
        </div>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
