import { handleActions } from 'redux-actions';
import { acAppReset, acSetAsideIsOpened, acToggleAsideIsOpened } from '../actions';

const initialState = {
  asideIsOpened: true,
};

export default handleActions({
  [acSetAsideIsOpened]: (state, { payload }) => ({
    ...state,
    asideIsOpened: payload,
  }),

  [acToggleAsideIsOpened]: (state) => ({
    ...state,
    asideIsOpened: !state.asideIsOpened,
  }),

  [acAppReset]: () => ({
    ...initialState,
  }),
}, initialState);
