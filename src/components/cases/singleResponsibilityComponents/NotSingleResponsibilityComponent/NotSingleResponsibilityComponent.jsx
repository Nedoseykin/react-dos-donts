import React from 'react';
import PropTypes from 'prop-types';

import { DEVELOPMENT, PRODUCTION } from '../../../../constants/modes.constants';

import styles from './NotSingleResponsibilityComponent.module.scss';

const NotSingleResponsibilityComponentInner = ({ mode }) => (
  mode === DEVELOPMENT
    ? <h3 className={styles.development}>Development mode</h3>
    : <h3 className={styles.production}>Production mode</h3>
);

const NotSingleResponsibilityComponent = ({ mode }) => {
  return (
    <>
      <h2>Currently application is working in </h2>
      <NotSingleResponsibilityComponentInner mode={mode} />
    </>
  )
};

NotSingleResponsibilityComponent.propTypes = {
  mode: PropTypes.oneOf([DEVELOPMENT, PRODUCTION]),
};

NotSingleResponsibilityComponent.defaultProps = {
  mode: DEVELOPMENT,
};

export default NotSingleResponsibilityComponent;
