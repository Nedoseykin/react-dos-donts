import React from 'react';
import { Switch, Route } from 'react-router';

import Page from '../Page';
import { SECTION_BY_PATHNAME } from '../../constants';

const sectionRoutes = Object.keys(SECTION_BY_PATHNAME);

const Router = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Page />
      </Route>

      <Route path={sectionRoutes}>
        <Page />
      </Route>

      <Route>
        <div>404 error. Something went wrong. Check the URL and try again</div>
      </Route>
    </Switch>
  )
};

export { Router };
