import React from 'react';
import * as PropTypes from 'prop-types';

import Question from '../../Question';

const Question1 = ({
  onChange,
  withCleansing,
}) => {
  return (
    <Question
      id="Question_1"
      label="Question 1"
      model="risk.question1"
      onChange={withCleansing ? onChange : null}
    />
  )
};

Question1.propTypes = {
  withCleansing: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

Question1.defaultProps = {
  withCleansing: false,
};

export { Question1 };
