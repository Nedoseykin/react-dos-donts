import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './SubSection.module.scss';

const SubSection = ({ children, condition }) => condition && (
  <div className={styles.Subsection}>
    {children}
  </div>
)

SubSection.propTypes = {
  children: PropTypes.any,
  condition: PropTypes.bool,
};

SubSection.defaultProps = {
  children: null,
  condition: true,
};

export { SubSection };
