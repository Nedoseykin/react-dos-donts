import get from 'lodash/get';

const pathNameSelector = state => get(state, 'router.location.pathname', '/');

export {
  pathNameSelector,
};
