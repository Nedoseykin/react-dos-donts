import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Section.module.scss';

const Section = ({
  title,
  children,
}) => {
  return (
    <section className={styles.Section}>
      <h2 className={styles.title}>{title}</h2>
      {children}
    </section>
  )
};

Section.propTypes = {
  title: PropTypes.string,
  children: PropTypes.any,
};

Section.defaultProps = {
  title: '',
  children: null,
};

export { Section };
