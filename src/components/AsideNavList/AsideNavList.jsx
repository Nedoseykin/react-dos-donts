import React from 'react';
// import * as PropTypes from 'prop-types';

import { NavLink } from 'react-router-dom';

import List from '../List';
import ListItem from '../ListItem';
import { SECTION_BY_PATHNAME } from '../../constants';

import styles from './AsideNavList.module.scss';

const AsideNavList = () => {
  const items = Object.keys(SECTION_BY_PATHNAME)
    .map(route => (
      <ListItem key={route} className={styles.listItem}>
        <NavLink
          to={route}
          className={styles.linkItem}
          activeClassName={styles.activeLinkItem}
        >
          {SECTION_BY_PATHNAME[route].linkLabel}
        </NavLink>
      </ListItem>
    ));

  return (
    <nav className={styles.AsideNavList}>
      <List>
        {items}
      </List>
    </nav>
  );
};

AsideNavList.propTypes = {};

AsideNavList.defaultProps = {};

export { AsideNavList };
