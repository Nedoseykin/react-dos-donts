import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Question.module.scss';

const Question = ({
  id,
  label,
  model,
  value,
  change,
  onChange,
}) => {
  const handleChange = (e) => {
    change(e);
    onChange && onChange(e);
  };

  return (
    <div className={styles.Question}>
      <label htmlFor={id} className={styles.label}>{ label }</label>
      <input
        className={styles.control}
        id={id}
        name={model}
        type="text"
        value={value}
        autoComplete="off"
        onChange={handleChange}
      />
    </div>
  )
};

Question.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  model: PropTypes.string.isRequired,
  value: PropTypes.string,
  change: PropTypes.func.isRequired,
  onChange: PropTypes.func,
};

Question.defaultProps = {
  id: '',
  label: '',
  value: '',
  onChange: null,
};

export { Question };
