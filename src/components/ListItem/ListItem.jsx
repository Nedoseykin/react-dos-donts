import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './ListItem.module.scss';

const ListItem = ({
  children,
  className,
}) => (
  <li className={classNames(styles.ListItem, className && className)}>
    {children}
  </li>
);

ListItem.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
};

ListItem.defaultProps = {
  children: null,
  className: '',
}

export { ListItem };
