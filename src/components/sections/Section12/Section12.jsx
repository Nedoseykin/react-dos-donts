import React from 'react';
import * as PropTypes from 'prop-types';

import Section from '../../Section';
import SubSection from '../../SubSection';

import TwoUseEffects from '../../cases/useEffect/TwoUseEffects/TwoUseEffects';
import OneUseEffects from '../../cases/useEffect/OneUseEffects/OneUseEffects';

const Section12 = () => {
  const handleSuccess = data => console.log('%c%s', 'color: green; font-weight: bold;', 'Success! Data was loaded...', data);

  return (
    <Section title="Section 12 - useEffect specific">
      <SubSection>
        <h3>useEffect specific</h3>
        <SubSection>
          <TwoUseEffects onSuccess={handleSuccess} />
        </SubSection>

        <SubSection>
          <OneUseEffects onSuccess={handleSuccess} />
        </SubSection>
      </SubSection>
    </Section>
  );
};

Section12.propTypes = {
  question1: PropTypes.string,
};

export { Section12 };
