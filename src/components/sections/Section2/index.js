import { connect } from 'react-redux';
import get from 'lodash/get';

import { Section2 } from './Section2';

const mapStateToProps = state => ({
  allAreVisible: get(state, 'data.risk.question1') === 'all',
  question2IsValid: Boolean(get(state, 'data.risk.question2')),
  question3IsValid: Boolean(get(state, 'data.risk.question3')),
  question4IsValid: Boolean(get(state, 'data.risk.question4')),
});

export default connect(mapStateToProps)(Section2);
