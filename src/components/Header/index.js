import { connect } from 'react-redux';

import { Header } from './Header';

import { acToggleAsideIsOpened } from '../../store/actions';

const mapDispatchToProps = {
  toggleAside: acToggleAsideIsOpened,
};

export default connect(null, mapDispatchToProps)(Header);
