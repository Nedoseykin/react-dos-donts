import React from 'react';
import * as PropTypes from 'prop-types';

import Section from '../../Section';
import SubSection from '../../SubSection';
import Question1 from '../../questions/Question1';
import Question2 from '../../questions/Question2';
import Question3 from '../../questions/Question3';
import Question4 from '../../questions/Question4';
import Question5 from '../../questions/Question5';
import QuestionsValidationList from '../../QuestionsValidationList';

import {
  question2IsValidMemoizedSelector,
  question3IsValidMemoizedSelector,
  question4IsValidMemoizedSelector,
} from '../../../store/selectors';

const validationListConfig = [
  { id: '1', label: 'Question 2', isValidSelector: question2IsValidMemoizedSelector },
  { id: '2', label: 'Question 3', isValidSelector: question3IsValidMemoizedSelector },
  { id: '3', label: 'Question 4', isValidSelector: question4IsValidMemoizedSelector },
];

const Section7 = ({ allAreVisible }) => {
  return (
    <Section title="Section 7 - every question was separated to component, section is responsible just for conditional rendering">
      <Question1 />

      <SubSection condition={allAreVisible}>
        <Question2 />

        <Question3 />

        <Question4 />

        <SubSection>
          <QuestionsValidationList config = {validationListConfig} />
        </SubSection>
      </SubSection>

      <Question5 />
    </Section>
  );
};

Section7.propTypes = {
  question1: PropTypes.string,
};

export { Section7 };
