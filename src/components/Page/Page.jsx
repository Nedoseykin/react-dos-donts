import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import get from 'lodash/get';

import AsideNavList from '../AsideNavList';

import { SECTION_BY_PATHNAME } from '../../constants';

import styles from './Page.module.scss';

const Page = ({
  asideIsOpened,
  pathName,
}) => {
  const PageSection = get(SECTION_BY_PATHNAME, [pathName, 'section']);

  return (
    <>
      <aside
        className={classNames(styles.Aside, asideIsOpened && styles.Aside_visible)}
      >
        <AsideNavList />
      </aside>

      <section className={classNames(styles.MainSection, asideIsOpened && styles.withAside)}>
        {PageSection && <PageSection />}
      </section>
    </>
  )
};

Page.propTypes = {
  asideIsOpened: PropTypes.bool,
  pageSection: PropTypes.any,
};

Page.defaultProps = {
  asideIsOpened: true,
  pageSection: null,
};

export { Page };
