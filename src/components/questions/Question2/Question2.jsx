import React from 'react';
import * as PropTypes from 'prop-types';

import Question from '../../Question';

const Question2 = ({
  onChange,
  withFragments,
}) => {
  return (
    <Question
      id="Question_2"
      label="Question 2"
      model="risk.question2"
      onChange={withFragments ? onChange : null}
    />
  )
};

Question2.propTypes = {
  withFragments: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

Question2.defaultProps = {
  withFragments: false,
};

export { Question2 };
