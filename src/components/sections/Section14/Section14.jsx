import React from 'react';
import * as PropTypes from 'prop-types';

import Section from '../../Section';
import SubSection from '../../SubSection';

import NotSingleResponsibilityUseEffect
  from '../../cases/singleResponsibilityUseEffect/NotSingleResponsibilityUseEffect/NotSingleResponsibilityUseEffect';
import SingleResponsibilityUseEffect
  from '../../cases/singleResponsibilityUseEffect/SingleResponsibilityUseEffect/SingleResponsibilityUseEffect';

const Section14 = () => {
  return (
    <Section title="Section 14 - Single responsibility useEffect">
      <SubSection>
        <h3>Not a single responsibility useEffect</h3>

        <NotSingleResponsibilityUseEffect />
      </SubSection>

      <SubSection>
        <h3>Single responsibility useEffect</h3>

        <SingleResponsibilityUseEffect />
      </SubSection>
    </Section>
  );
};

Section14.propTypes = {
  question1: PropTypes.string,
};

export { Section14 };
