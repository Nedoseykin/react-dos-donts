import { connect } from 'react-redux';

import { Question3 } from './Question3';

import { acChangeValue } from '../../../store/actions';
import { getFragmentsModel } from '../../../utils';

const mapDispatchToProps = {
  onChange: ({ target }) => acChangeValue({
    model: getFragmentsModel(target.name),
    value: target.value,
  }),
}

export default connect(null, mapDispatchToProps)(Question3);
