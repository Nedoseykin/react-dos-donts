import { connect } from 'react-redux';

import { Section4 } from './Section4';
import {
  allAreVisibleSelector,
  question2IsValidSelector,
  question3IsValidSelector,
  question4IsValidSelector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  allAreVisible: allAreVisibleSelector(state),

  question2IsValid: question2IsValidSelector(state),
  question3IsValid: question3IsValidSelector(state),
  question4IsValid: question4IsValidSelector(state),
});

export default connect(mapStateToProps)(Section4);
