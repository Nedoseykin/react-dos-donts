import { combineReducers } from 'redux';
import reduceReducers from 'reduce-reducers';

import routeReducer from './routeReducer';
import dataReducer from './dataReducer';
import ui from './uiReducer';

import dataFragmentsReducer from './dataFragmentsReducer';

export default function rootReducer(history) {
  return combineReducers({
    router: routeReducer(history),
    data: reduceReducers(dataReducer, dataFragmentsReducer),
    ui,
  });
}
