import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Header.module.scss';

const Header = ({
  children,
  toggleAside,
}) => {
  const handleHeaderClick = (e) => {
    e.stopPropagation();
    toggleAside();
  };

  return (
    <header
      className={styles.Header}
      onClick={handleHeaderClick}
    >
      { children }
    </header>
  );
};

Header.propTypes = {
  children: PropTypes.any,
  toggleAside: PropTypes.func,
};

Header.defaultProps = {
  toggleAside: () => {},
};

export { Header };
