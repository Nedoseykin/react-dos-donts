import { connect } from 'react-redux';

import { Section8 } from './Section8';
import { allAreVisibleSelector } from '../../../store/selectors';

const mapStateToProps = state => ({
  allAreVisible: allAreVisibleSelector(state),
});

export default connect(mapStateToProps)(Section8);
