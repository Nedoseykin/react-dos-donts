import { connect } from 'react-redux';

import { acUpdateStore } from '../../../store/actions';

import { Question1UpdateStore } from './Question1UpdateStore';
import {
  question2FragmentsSelector,
  question3FragmentsSelector,
  question4FragmentsSelector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  question2Fragments: question2FragmentsSelector(state),
  question3Fragments: question3FragmentsSelector(state),
  question4Fragments: question4FragmentsSelector(state),
});

const mapDispatchToProps = {
  onUpdateStore: acUpdateStore,
};

export default connect(mapStateToProps, mapDispatchToProps)(Question1UpdateStore);
