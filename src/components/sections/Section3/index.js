import { connect } from 'react-redux';

import { Section3 } from './Section3';
import {
  question1Selector,
  question2Selector,
  question3Selector,
  question4Selector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  allAreVisible: question1Selector(state) === 'all',
  question2IsValid: Boolean(question2Selector(state)),
  question3IsValid: Boolean(question3Selector(state)),
  question4IsValid: Boolean(question4Selector(state)),
});

export default connect(mapStateToProps)(Section3);
