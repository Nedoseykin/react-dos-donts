import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { callApi } from '../../../../utils';

import styles from './OneUseEffect.module.scss';

const OneUseEffects = ({ onSuccess }) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [shouldBeLoaded, setShouldBeLoaded] = useState(false);

  const loadData = useCallback(() => {
    setIsLoading(true);
    callApi()
      .then((response) => {
        setData(response);
        onSuccess(response);
      })
      .catch(err => setError(err))
      .finally(() => setIsLoading(false));
  }, [onSuccess]);

  useEffect(() => {
    if (shouldBeLoaded) {
      setShouldBeLoaded(false);
      loadData();
    }
  }, [shouldBeLoaded, loadData]);

  return (
    <>
      <div className={styles.root}>
        {isLoading ? 'Loading' : data || error}
      </div>
      {!isLoading && (
        <button
          className={styles.loadButton}
          onClick={() => setShouldBeLoaded(true)}
        >
          One useEffect - Load data
        </button>
      )}
    </>
  )
};

OneUseEffects.propTypes = {
  onSuccess: PropTypes.func.isRequired,
};

export default OneUseEffects;
