export const callApi = () => new Promise((resolve, reject) => {
  setTimeout(() => {
    const rnd = Math.round(Math.random() * 100);
    const isError = Boolean(rnd % 2);
    console.log('--------------------------------------------');
    console.log('Call API - ', isError ? 'Error' : 'Success');
    if (!isError) {
      resolve('Data from api');
    } else {
      reject('Something went wrong!');
    }
    console.log('--------------------------------------------');
  }, 2000);
});