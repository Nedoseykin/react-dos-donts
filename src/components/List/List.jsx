import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './List.module.scss';

const List = ({ children }) => (
  <ul className={styles.List}>
    {children}
  </ul>
);

List.propTypes = {
  children: PropTypes.any,
};

List.defaultProps = {
  children: null,
};

export { List };
