import React, { useState } from 'react';
import PropTypes from 'prop-types';

import styles from './NoNeedUseState.module.scss';

const NoNeedUseState = ({ submit }) => {
  const [count, setCount] = useState(0);

  return (
    <section className={styles.root}>
      <h3>useState is not necessary</h3>

      <div className={styles.toolbar}>
        <button onClick={() => setCount(count + 1)}>
          Increment
        </button>

        <button onClick={() => submit(count)}>
          Submit
        </button>
      </div>
    </section>
  )
};

NoNeedUseState.propTypes = {
  submit: PropTypes.func,
};

NoNeedUseState.defaultProps = {
  submit: () => {},
};

export default NoNeedUseState;
