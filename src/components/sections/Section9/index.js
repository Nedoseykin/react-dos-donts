import { connect } from 'react-redux';

import { Section9 } from './Section9';
import { allAreVisibleSelector } from '../../../store/selectors';

const mapStateToProps = state => ({
  allAreVisible: allAreVisibleSelector(state),
});

export default connect(mapStateToProps)(Section9);
