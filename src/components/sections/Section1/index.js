import { connect } from 'react-redux';
import get from 'lodash/get';

import { Section1 } from './Section1';

const mapStateToProps = state => ({
  question1: get(state, 'data.risk.question1'),
  question2: get(state, 'data.risk.question2'),
  question3: get(state, 'data.risk.question3'),
  question4: get(state, 'data.risk.question4'),
});

export default connect(mapStateToProps)(Section1);
