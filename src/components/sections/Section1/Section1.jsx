import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import Section from '../../Section';
import Question from '../../Question';

import styles from './Section1.module.scss';

const Section1 = ({
  question1,
  question2,
  question3,
  question4,
}) => {
  const allAreVisible = question1 === 'all';

  const getQuestionValidity = (label, value) => `${label} is ${value ? 'valid' : 'invalid'}`;

  const question2Validity = getQuestionValidity('Question 2', question2);
  const question3Validity = getQuestionValidity('Question 3', question3);
  const question4Validity = getQuestionValidity('Question 4', question4);

  const getMessageClassName = value => classNames(
    styles.messageItem,
    value ? styles.validMessage : styles.invalidMessage,
  );

  return (
    <Section title="Section 1 - props are AS IS in the state`s models (String)">
      <Question
        id="Question_1"
        label="Question 1"
        model="risk.question1"
      />

      {
        allAreVisible && (
          <>
            <Question
              id="Question_2"
              label="Question 2"
              model="risk.question2"
            />

            <Question
              id="Question_3"
              label="Question 3"
              model="risk.question3"
            />

            <Question
              id="Question_4"
              label="Question 4"
              model="risk.question4"
            />

            <div className={styles.message_box}>
              <h4 key="2" className={getMessageClassName(question2)}>
                {question2Validity}
              </h4>
              <h4 key="3" className={getMessageClassName(question3)}>
                {question3Validity}
              </h4>
              <h4 key="4" className={getMessageClassName(question4)}>
                {question4Validity}
              </h4>
            </div>
          </>
        )
      }

      <Question
        id="Question_5"
        label="Question 5"
        model="risk.question5"
      />
    </Section>
  );
};

Section1.propTypes = {
  question1: PropTypes.string,
};

export { Section1 };
