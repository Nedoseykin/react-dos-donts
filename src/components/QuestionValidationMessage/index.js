import { connect } from 'react-redux';

import { QuestionValidationMessage } from './QuestionValidationMessage';

const mapStateToProps = (state, ownProps) => ({
  isValid: ownProps.isValidSelector && ownProps.isValidSelector(state),
});

export default connect(mapStateToProps)(QuestionValidationMessage);
