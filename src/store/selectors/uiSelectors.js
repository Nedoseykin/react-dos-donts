import get from 'lodash/get';

const asideIsOpenedSelector = state => get(state, 'ui.asideIsOpened', true);

export {
  asideIsOpenedSelector,
};
