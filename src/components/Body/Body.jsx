import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Body.module.scss';

const Body = ({ children }) => {
  return (
    <div className={styles.Body}>
      { children }
    </div>
  )
};

Body.propTypes = {
  children: PropTypes.any,
};

Body.defaultProps = {};

export { Body };
