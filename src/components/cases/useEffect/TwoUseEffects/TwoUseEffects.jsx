import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { callApi } from '../../../../utils';

import styles from './TwoUseEffect.module.scss';

const TwoUseEffects = ({ onSuccess }) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [shouldBeLoaded, setShouldBeLoaded] = useState(false);

  const loadData = useCallback(() => {
    setIsLoading(true);
    callApi()
      .then(response => setData(response))
      .catch(err => setError(err))
      .finally(() => setIsLoading(false));
  }, []);

  useEffect(() => {
    if (shouldBeLoaded) {
      setShouldBeLoaded(false);
      loadData();
    }
  }, [shouldBeLoaded, loadData]);

  useEffect(() => {
    if (!isLoading && !error && data) {
      onSuccess(data);
    }
  }, [isLoading, error, data, onSuccess]);

  return (
    <>
      <div className={styles.root}>
        {isLoading ? 'Loading' : data || error}
      </div>
      {!isLoading && (
        <button
          className={styles.loadButton}
          onClick={() => setShouldBeLoaded(true)}
        >
          Two useEffects - Load data
        </button>
      )}
    </>
  )
};

TwoUseEffects.propTypes = {
  onSuccess: PropTypes.func.isRequired,
};

export default TwoUseEffects;
