import { handleActions } from 'redux-actions';
import get from 'lodash/get';
import set from 'lodash/set';
import unset from 'lodash/unset';
import cloneDeep from 'lodash/cloneDeep';

import { acChangeQuestion1Value } from '../actions';
import { getFragmentsModel } from '../../utils';

// const question2model = 'risk.question2';
// const question3model = 'risk.question3';
// const question4model = 'risk.question4';

const initialState = {};

export default handleActions({
  [acChangeQuestion1Value]: (state, { payload }) => {
    const { value } = payload;

    const newState = cloneDeep(state);
    const modelList = [
      'risk.question2',
      'risk.question3',
      'risk.question4',
    ];

    if (value === 'all') {
      modelList
        .map(model => ({
          model,
          value: get(newState, getFragmentsModel(model)),
        }))
        .forEach(item => (item.value && set(newState, item.model, item.value)));
    } else {
      modelList
        .filter(model => get(newState, model) !== undefined)
        .forEach(model => unset(newState, model));
    }

    return newState;
  },
}, initialState);
