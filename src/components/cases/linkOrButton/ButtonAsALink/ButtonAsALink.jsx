import React from 'react';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';

import styles from './ButtonAsALink.module.scss';

const ButtonAsALink = ({ to, children }) => {
  const history = useHistory();

  return (
    <button
      className={styles.root}
      onClick={() => history.push(to)}
    >
      {children}
    </button>
  )
};

ButtonAsALink.propTypes = {
  to: PropTypes.string,
};

ButtonAsALink.defaultProps = {
  to: '#',
};

export default ButtonAsALink;
