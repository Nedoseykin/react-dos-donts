import React from 'react';
import * as PropTypes from 'prop-types';

import Question from '../../Question';

const Question4 = ({ onChange, withFragments }) => {
  return (
    <Question
      id="Question_4"
      label="Question 4"
      model="risk.question4"
      onChange={withFragments ? onChange : null}
    />
  )
};

Question4.propTypes = {
  withFragments: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

Question4.defaultProps = {
  withFragments: false,
};

export { Question4 };
