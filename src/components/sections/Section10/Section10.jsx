import React from 'react';
import * as PropTypes from 'prop-types';

import Section from '../../Section';
import SubSection from '../../SubSection';
import NoNeedUseState from '../../cases/useState/NoNeedUseState/NoNeedUseState';
import NoNeedUseStateCorrect from '../../cases/useState/NoNeedUseStateCorrect/NoNeedUseStateCorrect';

const Section10 = ({ submit }) => {
  return (
    <Section title="Section 10 - Usage of useState">
      <SubSection>
        <SubSection>
          <NoNeedUseState submit={submit} />
        </SubSection>

        <SubSection>
          <NoNeedUseStateCorrect submit={submit} />
        </SubSection>
      </SubSection>
    </Section>
  );
};

Section10.propTypes = {
  submit: PropTypes.func.isRequired,
};

export { Section10 };
