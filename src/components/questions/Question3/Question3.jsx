import React from 'react';
import * as PropTypes from 'prop-types';

import Question from '../../Question';

const Question3 = ({ onChange, withFragments }) => {
  return (
    <Question
      id="Question_3"
      label="Question 3"
      model="risk.question3"
      onChange={withFragments ? onChange : null}
    />
  )
};

Question3.propTypes = {
  withFragments: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

Question3.defaultProps = {
  withFragments: false,
};

export { Question3 };
