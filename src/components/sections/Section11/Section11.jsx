import React from 'react';

import Section from '../../Section';
import SubSection from '../../SubSection';

import ButtonAsALink from '../../cases/linkOrButton/ButtonAsALink/ButtonAsALink';
import LinkAsAButton from '../../cases/linkOrButton/LinkAsAButton/LinkAsAButton';

const Section11 = () => {
  return (
    <Section title="Section 11 - History.push vs. Link with anchor DOM-element">
      <SubSection>
        <h3>History.push</h3>

        <ButtonAsALink to="#">
          I am a button - Go to #
        </ButtonAsALink>
      </SubSection>

      <SubSection>
        <h3>Link with anchor DOM-element</h3>

        <LinkAsAButton to="#">
          I am a link - Go to #
        </LinkAsAButton>
      </SubSection>
    </Section>
  );
};

export { Section11 };
