import { connectRouter } from 'connected-react-router';

export default function(history) {
  return connectRouter(history);
}
